#include<iostream>

using namespace std;

class Graph{
    int n;
    // create an array pointer since we don't use standard method;
    int * arr;
    int * sizes;
private:
    int get_root_recursive(int start){
        if(start==arr[start]){
            return  start;
        }
        else{
            return  get_root(arr[start]);
        }
    }
    int get_root(int start){
        while(start!=arr[start]){
            start=arr[start];
        }
        return start;
    }

    int get_root_improved(int start){
        while(start!=arr[start]){
            arr[start]=arr[arr[start]];
            start=arr[start];
        }
        return start;
    }
public:
    Graph(int size){
        n=size;
        arr=(int *)calloc(sizeof(int),n);
        sizes=(int *)calloc(sizeof(int),n);
        for(int i=0;i<n;i++){
            arr[i]=i;
            sizes[i]=0;
        }
    };

    void un(int i,int j){
        int first_root=get_root(i);
        int second_root=get_root(j);
        arr[first_root]=second_root;
    }

    void un_improved(int i,int j){
        int first_root=get_root_improved(i);
        int second_root=get_root_improved(j);

        if(sizes[second_root]>sizes[first_root]) {
            arr[first_root]=second_root;
            sizes[second_root]+=sizes[first_root];
        }
        else {
            arr[second_root] = first_root;
            sizes[first_root]+=sizes[second_root];
        }
    }

    void display(){
        for(int k=0;k<n;k++){
            cout<<arr[k]<<" ";
        }
    };

    bool find(int i,int j){
        int first_root=get_root_improved(i);
        int second_root=get_root_improved(j);
        return  first_root==second_root;
    }
};

int main(){
    Graph graph(10);
    graph.un_improved(0,8);
    graph.un_improved(8,7);
    graph.un_improved(5,8);
//    graph.display();
   cout<<graph.find(0,5);
}