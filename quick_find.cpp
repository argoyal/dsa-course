#include<iostream>
using namespace std;

class Graph{
    int n;
    int * arr;
public:
    Graph(int size){
        n=size;
        arr=(int *)calloc(sizeof(int),n);
        for(int i=0;i<n;i++){
            arr[i]=i;
        }
    };
    void un(int i,int j){
        int change=arr[i];
        int old=arr[j];
        for(int k=0;k<n;k++){
            if(arr[k]==old){
                arr[k]=change;
            }
        }
    };
    void display(){
        for(int k=0;k<n;k++){
            cout<<arr[k]<<" ";
        }
    };

    bool find(int i,int j){
        if(i>n-1 || j>n-1){
            return false;
        }
        return arr[i]==arr[j];
    }
};

int main(){
Graph g(10);
g.un(0,8);
cout<<g.find(0,8);
}
